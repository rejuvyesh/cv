#!/bin/sh
#
# File: sendcv.sh
#
# Created: Thursday, October 05 2014 by rejuvyesh
#

title='<meta name="google-site-verification" content="_jHgqYq-rsboPyW-xiHZLzYB5_j_crrXYXsfELg9hlc" /><title>Jayesh Kumar Gupta - CV</title>'
includecss='<link rel="stylesheet" type="text/css" href="/css/style.css" /> \n <link rel="stylesheet" type="text/css" href="/css/base.css" />'
cssbefore='<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">'

containerdiv='<div id="page-container">'
homelink='<p style="text-align:center;"> <a href="/"><span aria-hidden="true" class="icon-home"> Home</a></p>'
pdflink='<p style="text-align:center;"><span>Get <a href="/files/jayeshkg-cv.pdf">PDF <span aria-hidden="true" class="icon-file-pdf"></span></a> version.</span></p>'


cp cv.pdf ~/src/www/rejuvyesh.com/files/jayeshkg-cv.pdf 
echo "pdf copied to website"

pdf2htmlEX --embed-outline 0 --data-dir ~/local/share/pdf2htmlEX --zoom 1.5 --hdpi 292 --vdpi 292 cv.pdf

sed -i "s#$cssbefore#$title\n$includecss\n$cssbefore#" cv.html
sed -i "s#$containerdiv#$containerdiv\n$homelink\n$pdflink#" cv.html

echo "html version done"

cp cv.html ~/src/www/rejuvyesh.com/
